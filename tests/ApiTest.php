<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class ApiTest extends TestCase
{
    /**
     * test endpoints
     *
     * @return void
     */
    public function testEndpoint()
    {
        $payload = array (
            'providers' =>
                array (
                    0 =>
                        array (
                            'name' => 'GLL/Better',
                            'blocks' =>
                                array (
                                    0 =>
                                        array (
                                            'start' => '2016-05-16 06:00:00.0',
                                            'end' => '2016-05-16 23:00:00.0',
                                            'slots' =>
                                                array (
                                                    0 =>
                                                        array (
                                                            'id' => 'slot216823',
                                                            'sport' => 'Football',
                                                            'format' => 'Outdoor',
                                                            'venue' => 'Islington Parks',
                                                            'pitch' => '',
                                                            'start' => '2016-05-12 10:00:00.0',
                                                            'end' => '2016-05-12 11:00:00.0',
                                                            'availability' => '2',
                                                            'price' => 45.25,
                                                        ),
                                                    1 =>
                                                        array (
                                                            'id' => 'slot202552',
                                                            'sport' => 'Squash',
                                                            'format' => 'Indoor',
                                                            'venue' => 'Sobell LC',
                                                            'pitch' => '',
                                                            'start' => '2016-05-12 16:40:00.0',
                                                            'end' => '2016-05-12 17:40:00.0',
                                                            'availability' => '1',
                                                            'price' => 9.8000000000000007,
                                                        ),
                                                ),
                                        ),
                                ),
                        ),
                ),
        );

        $response = $this->call('POST', '/api/create/provider', $payload);

        $this->assertEquals(200,  $response->getStatusCode());

        $this->assertTrue($response->isOk());

        $this->seeInDatabase('slot', ['uid' => 'slot202552']);

        $this->assertEquals('success', $response->getContent());
    }
}
