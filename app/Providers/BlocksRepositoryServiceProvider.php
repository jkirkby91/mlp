<?php

namespace App\Providers;

use App\Entities\Blocks;
use App\Repositories\BlocksRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class BlocksRepositoryServiceProvider
 *
 * @package App\Providers
 * @author James Kirkby <jkirkby91@gmail.com>
 */
class BlocksRepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BlocksRepository::class, function($app) {
            // This is what Doctrine's EntityRepository needs in its constructor.
            return new BlocksRepository(
                $app['em'],
                $app['em']->getClassMetaData(Blocks::class)
            );
        });
    }

    /**
     * Get the services provided by the provider since we are deferring load.
     *
     * @return array
     */
    public function provides()
    {
        return ['App\Repositories\BlocksRepository'];
    }
}