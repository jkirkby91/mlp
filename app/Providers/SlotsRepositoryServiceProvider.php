<?php

namespace App\Providers;

use App\Entities\Slot;
use App\Repositories\SlotsRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class SlotsRepositoryServiceProvider
 *
 * @package App\Providers
 * @author James Kirkby <jkirkby91@gmail.com>
 */
class SlotsRepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SlotsRepository::class, function($app) {
            // This is what Doctrine's EntityRepository needs in its constructor.
            return new SlotsRepository(
                $app['em'],
                $app['em']->getClassMetaData(Slot::class)
            );
        });
    }

    /**
     * Get the services provided by the provider since we are deferring load.
     *
     * @return array
     */
    public function provides()
    {
        return ['App\Repositories\SlotsRepository'];
    }
}