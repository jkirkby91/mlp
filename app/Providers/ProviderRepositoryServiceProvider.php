<?php

namespace App\Providers;

use App\Entities\Provider;
use App\Repositories\ProviderRepository;
use Illuminate\Support\ServiceProvider;

/**
 * Class ProviderRepositoryServiceProvider
 *
 * @package App\Providers
 * @author James Kirkby <jkirkby91@gmail.com>
 */
class ProviderRepositoryServiceProvider extends ServiceProvider
{
    protected $defer = true;

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(ProviderRepository::class, function($app) {
            // This is what Doctrine's EntityRepository needs in its constructor.
            return new ProviderRepository(
                $app['em'],
                $app['em']->getClassMetaData(Provider::class)
            );
        });
    }

    /**
     * Get the services provided by the provider since we are deferring load.
     *
     * @return array
     */
    public function provides()
    {
        return ['App\Repositories\ProviderRepository'];
    }
}