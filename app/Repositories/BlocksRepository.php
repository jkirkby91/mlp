<?php

namespace App\Repositories;

use App\Entities\Blocks;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;

/**
 * Class BlocksRepository
 *
 * Repository for Blocks Entity
 *
 * @package App\Repositories
 * @author James Kirkby <jkirkby91@gmail.com>
 */
class BlocksRepository extends EntityRepository
{

    /**
     * @param $block
     */
    public function create($block)
    {
        $blockEntity = new Blocks($block['start'],$block['end']);

        $this->_em->persist($blockEntity);
        $this->_em->flush();
    }
}
