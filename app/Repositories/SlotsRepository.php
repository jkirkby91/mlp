<?php

namespace App\Repositories;

use App\Entities\Slot;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMInvalidArgumentException;

/**
 * Class SlotsRepository
 *
 * Repository for Blocks Entity
 *
 * @package App\Repositories
 * @author James Kirkby <jkirkby91@gmail.com>
 */
class SlotsRepository extends EntityRepository
{

    /**
     * @param $slot
     */
    public function create($slot)
    {
        $slotEntity = new Slot($slot['id'],$slot['sport'],$slot['format'],$slot['venue'],$slot['pitch'],$slot['start'],$slot['end'],$slot['availability'],$slot['price']);

        $this->_em->persist($slotEntity);
        $this->_em->flush();
    }

}