<?php

namespace App\Repositories;

use App\Entities\Blocks;
use App\Entities\Provider;
use App\Entities\Slot;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\ORMInvalidArgumentException;

/**
 * Class ProviderRepository
 *
 * Repository for Provider Entity
 *
 * @package App\Repositories
 * @author James Kirkby <jkirkby91@gmail.com>
 */
class ProviderRepository extends EntityRepository
{

    /**
     * @param $provider
     * @return Provider|string
     */
    public function importProviders($provider)
    {
        $providerEntity = new Provider($provider['name']);

        foreach ($provider['blocks'] as $block)
        {
            $blockEntity = new Blocks($block['start'],$block['end']);

            foreach ($block['slots'] as $slot)
            {
                $slotEntity = new Slot($slot['id'],$slot['sport'],$slot['format'],$slot['venue'],$slot['pitch'],$slot['start'],$slot['end'],$slot['availability'],$slot['price']);

                $blockEntity->addSlot($slotEntity);
            }

            $providerEntity->addBlock($blockEntity);
        }

        try {
            $this->_em->persist($providerEntity);
            $this->_em->flush();

        } catch(ORMException $e) {
            throw new \LogicException($e->getMessage());
        }
        
        return $providerEntity;
    }
}