<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Providers
 *
 * Entity for providers
 *
 * @ORM\Entity
 * @ORM\Table(name="provider")
 *
 * @package App\Http\Controllers
 * @author James Kirkby <jkirkby91@gmail.com>
 */
class Provider
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\OneToMany(targetEntity="Blocks", mappedBy="provider", cascade={"persist"})
     * @var ArrayCollection|blocks[]
     */
    protected $blocks;

    /**
     * Providers constructor.
     *
     * @param $name
     */
    public function __construct($name)
    {
        $this->name = $name;

        $this->blocks = new ArrayCollection;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param $name
     * @return $this
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @param Blocks $blocks
     */
    public function addBlock(Blocks $blocks)
    {
        if(!$this->blocks->contains($blocks)) {
            $blocks->setProvider($this);
            $this->blocks->add($blocks);
        }
    }

    /**
     * @return mixed
     */
    public function getBlock()
    {
        return $this->blocks;
    }
}
