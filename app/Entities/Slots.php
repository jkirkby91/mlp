<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Slot
 *
 * Entity for slot
 *
 * @ORM\Entity
 * @ORM\Table(name="slot")
 *
 * @package App\Http\Controllers
 * @author James Kirkby <jkirkby91@gmail.com>
 */
class Slot
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $uid;

    /**
     * @ORM\Column(type="string")
     */
    protected $sport;

    /**
     * @ORM\Column(type="string")
     */
    protected $format;

    /**
     * @ORM\Column(type="string")
     */
    protected $venue;

    /**
     * @ORM\Column(type="string")
     */
    protected $pitch;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $start;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $end;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $availability;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=2)
     */
    protected $price;

    /**
     * @ORM\ManyToOne(targetEntity="Blocks", inversedBy="slots")
     * @var Blocks
     */
    protected $blocks;

    /**
     * Slot constructor.
     * @param $uid
     * @param $sport
     * @param $format
     * @param $venue
     * @param $pitch
     * @param $start
     * @param $end
     * @param $availability
     * @param $price
     */
    public function __construct($uid,$sport, $format, $venue, $pitch, $start, $end, $availability, $price)
    {
        $this->uid = $uid;
        $this->sport = $sport;
        $this->format = $format;
        $this->venue = $venue;
        $this->pitch = $pitch;
        $this->start = new \DateTime($start);
        $this->end = new \DateTime($end);
        $this->availability = $availability;
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return mixed
     */
    public function setUid()
    {
        return $this->uid;
    }

    /**
     * @return mixed
     */
    public function getSport()
    {
        return $this->sport;
    }

    /**
     * @param $sport
     * @return $this
     */
    public function setSport($sport)
    {
        $this->sport = $sport;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * @param $format
     * @return $this
     */
    public function setFormat($format)
    {
        $this->format = $format;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getVenue()
    {
        return $this->venue;
    }

    /**
     * @param $venue
     * @return $this
     */
    public function setVenue($venue)
    {
        $this->venue = $venue;
        return $this;
    }

    /**
    * @return mixed
    */
    public function getPitch()
    {
        return $this->pitch;
    }

    /**
     * @param $pitch
     * @return $this
     */
    public function setPitch($pitch)
    {
        $this->pitch = $pitch;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param $start
     * @return $this
     */
    public function setStart($start)
    {
        $this->start = new \DateTime($start);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param $end
     * @return $this
     */
    public function setEnd($end)
    {
        $this->end = new \DateTime($end);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAvailability()
    {
        return $this->availability;
    }

    /**
     * @param $availability
     * @return $this
     */
    public function setAvailability($availability)
    {
        $this->availability = $availability;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param $price
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @param Blocks $blocks
     */
    public function setBlocks(Blocks $blocks)
    {
        $this->blocks = $blocks;
    }

    /**
     * @return Blocks
     */
    public function getBlocks()
    {
        return $this->blocks;
    }
}
