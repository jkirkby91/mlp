<?php

namespace App\Entities;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping AS ORM;

/**
 * Class Blocks
 *
 * Entity for blocks
 *
 * @package App\Entities
 * @ORM\Entity
 * @ORM\Table(name="blocks")
 *
 * @package App\Http\Controllers
 * @author James Kirkby <jkirkby91@gmail.com>
 */
class Blocks
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id;

    /**
     * @ORM\Column(type="date")
     */
    protected $start;

    /**
     * @ORM\Column(type="date")
     */
    protected $end;

    /**
     * @ORM\OneToMany(targetEntity="Slot", mappedBy="blocks", cascade={"persist"})
     * @var ArrayCollection|slot[]
     */
    protected $slot;

    /**
     * @ORM\ManyToOne(targetEntity="Provider", inversedBy="blocks")
     * @var blocks
     */
    protected $provider;

    /**
     * Blocks constructor.
     * @param $start
     * @param $end
     */
    public function __construct($start, $end)
    {
        $this->start = new \DateTime($start);
        $this->end = new \DateTime($end);

        $this->slot = new ArrayCollection;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param $start
     * @return $this
     */
    public function setStart($start)
    {
        $this->start = new \DateTime($start);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param $end
     * @return $this
     */
    public function setEnd($end)
    {
        $this->end = new \DateTime($end);
        return $this;
    }

    /**
     * @param Slot $slot
     */
    public function addSlot(Slot $slot)
    {
        if(!$this->slot->contains($slot)) {
            $slot->setBlocks($this);
            $this->slot->add($slot);
        }
    }

    /**
     * @return mixed
     */
    public function getSlot()
    {
        return $this->slot;
    }

    /**
     * @param Provider $provider
     */
    public function setProvider(Provider $provider)
    {
        $this->provider = $provider;
    }

    /**
     * @return Blocks
     */
    public function getProvider()
    {
        return $this->provider;
    }
}
