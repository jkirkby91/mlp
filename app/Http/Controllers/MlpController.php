<?php

namespace App\Http\Controllers;

use App\Repositories\ProviderRepository;
use App\Repositories\BlocksRepository;
use App\Repositories\SlotsRepository;
use Doctrine\ORM\EntityManager;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Http\Response;

/**
 * Class MlpController
 *
 * End point to post Json data to create import pitch provider data
 *
 * @package App\Http\Controllers
 * @author James Kirkby <jkirkby91@gmail.com>
 */
class MlpController extends Controller
{

    /**
     * @var ProviderRepository
     */
    protected $providerRepository;

    /**
     * @var
     */
    protected $blocksRepository;

    /**
     * MlpController constructor.
     *
     * @param ProviderRepository $providerRepository
     * @param BlocksRepository $blocksRepository
     * @param SlotsRepository $slotsRepository
     */
    public function __construct(ProviderRepository $providerRepository, BlocksRepository $blocksRepository, SlotsRepository $slotsRepository)
    {
        $this->providerRepository = $providerRepository;
        $this->blocksRepository = $blocksRepository;
    }

    /**
     * @param Request $request
     * @return bool
     */
    public function createProvider(Request $request)
    {
        $request = $request->all();

        if(is_array($request) && empty($request))
        {
            throw new \LogicException('empty request');
        }

        foreach ($request['providers'] as $provider)
        {
            try {
                $providerEntity = $this->providerRepository->importProviders($provider);
            } catch(\LogicException $e) {
                throw new \LogicException($e->getMessage());
            }
        }

        if(isset($providerEntity))
        {
            return 'success';
        }

        return false;
    }
}
